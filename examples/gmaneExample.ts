import Client, { Options } from '../src/'

const options: Options = {
  host: 'news.gmane.org',
  responseInterceptor: response => {
    console.log(`[nntp] ${new Date().toISOString()}: ${JSON.stringify(response, undefined, 2)}`)
    return response
  },
  tlsOptions: {
    rejectUnauthorized: false // Accept GMANE's self-signed certificate
  }
}
const client = new Client(options)

const main = async (): Promise<void> => {
  await client.connect()

  let response = await client.capabilities()
  if (!response.capabilities.STARTTLS) {
    console.warn("Server doesn't advertise STARTTLS, possible MITM downgrade attack")
  }
  const { socket } = await client.startTls()

  // Verify fingerprint (since it's self-signed we should really add the CA but let's check the fingerprint for now)
  const fingerprint = socket.getPeerCertificate().fingerprint
  if (fingerprint !== '92:A2:44:DB:6E:33:22:7E:54:4C:C2:C1:7B:09:A3:BE:35:80:5F:08') {
    throw new Error('Fingerprint changed!')
  }

  // Warn the user about the self-signed cert
  if (socket.authorized === false) {
    console.warn('WARNING: Self-signed cert', socket.authorizationError)
  }

  response = await client.capabilities()
  if (response.capabilities.STARTTLS) {
    throw new Error('Server must not advertise STARTTLS anymore')
  }

  // Example list newsgroups
  response = await client.listNewsgroups('gmane.org*gnu*')

  // Example pipelining
  response = await Promise.all(
    response.newsgroups.slice(0, 3).map(newsgroup => client.listGroup(newsgroup.name))
  )

  // Example pipelining to get 5 articles from first group
  response = await Promise.all(
    response[0].articleNumbers.slice(0, 5).map(async articleNumber => client.article(articleNumber))
  )

  return client.quit()
}

main().catch(err => {
  console.error(err)
  client.disconnect()
})
