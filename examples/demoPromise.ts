import Client from '../src/'

const client = new Client({
  host: 'news.easynews.com',
  port: 110,
  responseInterceptor: response => {
    console.log(`[nntp] ${new Date().toISOString()}: ${JSON.stringify(response, undefined, 2)}`)
    return response
  }
})

client
  .connect()
  .then(() => {
    return client.capabilities()
  })
  .then(() => {
    return client.quit()
  })
  .catch(err => {
    console.error('Caught error', err)
    client.disconnect()
  })
