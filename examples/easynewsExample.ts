import Client from '../src/'

// TODO: try easynews ipv6 example

const client = new Client({
  host: 'secure.news.easynews.com',
  port: 21,
  tlsPort: true,
  responseInterceptor: response => {
    console.log(`[nntp] ${new Date().toISOString()}: ${JSON.stringify(response, undefined, 2)}`)
    return response
  }
})

const main = async (): Promise<void> => {
  const { socket } = await client.connect()
  if (!socket.encrypted) {
    throw new Error('We have a regular socket and not a TLS socket')
  }
  if (!socket.authorized) {
    throw new Error('peer certificate not signed by a CA')
  }
  const fingerprint = socket.getPeerCertificate().fingerprint
  if (fingerprint !== 'BE:BF:C1:F9:BC:B0:CC:6B:FB:C7:42:40:DF:9D:DE:CA:BD:0B:39:13') {
    console.error(`Fingerprint is ${fingerprint}`)
    throw new Error('Fingerprint changed!')
  }

  await client.capabilities()

  // Example login
  // let response = await client.authInfoUser('my_username')
  // await response.authInfoPass('my_password')

  await client.capabilities()
  // TODO: do stuff now that you're logged in

  return client.quit()
}

main().catch(err => {
  console.error(err)
  client.disconnect()
})
