import Client from '../src/'

const client = new Client({
  host: 'news.easynews.com',
  port: 110,
  responseInterceptor: response => {
    console.log(`[nntp] ${new Date().toISOString()}: ${JSON.stringify(response, undefined, 2)}`)
    return response
  }
})

const main = async () => {
  await client.connect()
  await client.capabilities()
  await client.quit()
}

main().catch(err => {
  console.error(err)
  client.disconnect()
})
