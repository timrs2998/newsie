import * as net from 'net'
import Client from '../src/Client'

jest.mock('net')
jest.mock('tls')

test('should initialize', () => {
  const host = 'google.com'
  const port = 110

  const client = new Client({ host, port })

  expect(client._connection['_host']).toEqual(host)
  expect(client._connection['_port']).toEqual(port)
  expect(client._connection['_queue']).toEqual([])
  expect(client._connection['_frames']).toEqual('')
})

describe('5. Session Administration Commands', () => {
  let client

  beforeEach(() => {
    client = new Client({
      host: 'hello',
      port: 1234
    })
  })

  describe('5.1. Initial Connection', () => {
    test.skip('200 Service available, posting allowed', async () => {
      await client.connect()

      expect(net.createConnection).toHaveBeenCalled()
      expect(net['_socket'].on).toBeCalledWith('data', expect.any(Function))
      expect(net['_socket'].once).toBeCalledWith('close', expect.any(Function))
      expect(client._connection._queue).toEqual([
        {
          callback: expect.any(Function),
          resolve: expect.any(Function),
          reject: expect.any(Function)
        }
      ])
    })
  })
})

test.skip('lines starting with a dot in the original text have that dot doubled during transmission', () => {
  // TODO: test POST / IHAVE commands sending articles with dots
})
