// import matches from '../wildmat'

describe('4.4.  Examples', () => {
  // In these examples, $ and @ are used to represent the two octets %xC2
  // and %xA3, respectively; $@ is thus the UTF-8 encoding for the pound
  // sterling symbol, shown as # in the descriptions.

  //   Wildmat    Description of strings that match
  //     abc      The one string "abc"
  //     abc,def  The two strings "abc" and "def"
  //     $@       The one character string "#"
  //     a*       Any string that begins with "a"
  //     a*b      Any string that begins with "a" and ends with "b"
  //     a*,*b    Any string that begins with "a" or ends with "b"
  //     a*,!*b   Any string that begins with "a" and does not end with
  //              "b"
  //   a*,!*b,c*  Any string that begins with "a" and does not end with
  //              "b", and any string that begins with "c" no matter
  //              what it ends with
  //   a*,c*,!*b  Any string that begins with "a" or "c" and does not
  //              end with "b"
  //     ?a*      Any string with "a" as its second character
  //     ??a*     Any string with "a" as its third character
  //     *a?      Any string with "a" as its penultimate character
  //     *a??     Any string with "a" as its antepenultimate character

  test('dummy', () => {
    // dummy test
  })
})
