import { fetch } from '../src'
import { Server } from '../src/MockServer'

describe('rfc 5538', () => {
  let server

  beforeEach(() => {
    server = new Server()
    return server.listen()
  })

  afterEach(() => server.close())

  test('fetching article from server', async () => {
    // given
    const port = server.getListeningPort()
    server.addSimpleHandler((command, args) => {
      if (command === 'CAPABILITIES') {
        return '101 Capability List:\r\nVERSION 2\r\n.\r\n'
      }
      expect(command).toEqual('ARTICLE')
      expect(args).toEqual(['12345'])
      return (
        '220 3000234 <45223423@example.com>\r\n' +
        'Path: pathost!demo!whitehouse!not-for-mail\r\n' +
        'From: "Demo User" <nobody@example.net>\r\n' +
        'Newsgroups: misc.test\r\n' +
        'Subject: I am just a test article\r\n' +
        'Date: 6 Oct 1998 04:38:40 -0500\r\n' +
        'Organization: An Example Net, Uncertain, Texas\r\n' +
        'Message-ID: <45223423@example.com>\r\n' +
        '\r\n' +
        'This is just a test article.\r\n' +
        '.\r\n'
      )
    })
    const uri = `nntp://localhost:${port}/example.group.this/12345`

    // when
    const article = await fetch(uri)

    // then
    expect(article).toEqual({
      code: 220,
      comment: '',
      description: 'Article follows (multi-line)',
      article: {
        articleNumber: 3000234,
        messageId: '<45223423@example.com>',
        headers: {
          PATH: 'pathost!demo!whitehouse!not-for-mail',
          FROM: '"Demo User" <nobody@example.net>',
          NEWSGROUPS: 'misc.test',
          SUBJECT: 'I am just a test article',
          DATE: '6 Oct 1998 04:38:40 -0500',
          ORGANIZATION: 'An Example Net, Uncertain, Texas',
          'MESSAGE-ID': '<45223423@example.com>'
        },
        body: ['This is just a test article.']
      }
    })
  })

  test.skip(`A <newsgroup-name> as specified in [RFC5536] consists of dot-
   separated components.  Each component contains one or more letters,
   digits, "-" (hyphen-minus), "+", or "_" (underscore).  These
   characters can be directly used in a segment of a path in an
   [RFC3986] URI; no percent-encoding is necessary.  Example:

       nntp://news.server.example/example.group.this/12345`, async () => {
    const uri = 'nntp://news.server.example/example.group.this/12345'
  })

  test.skip(`A <wildmat-exact> newsgroup name as specified in [RFC3977] allows (in
   theory) any <UTF8-non-ascii> (see Section 6) and most printable
   US-ASCII characters, excluding "!", "*", ",", "?", "[", "\\", and "]".
   However, [RFC5536] does not (yet) permit characters outside of
   <group-char> and so, to keep the syntax simple, the additional
   characters are here covered by <pct-encoded> as defined in [RFC3986],
   since most of them have to be percent-encoded anyway (with a few
   exceptions, such as ":", ";", and "~").  Example:`, () => {
    const uri = 'nntp://wild.server.example/example.group.n%2Fa/12345'
  })

  test.skip(`In the form without <article-number>, the URL identifies a single
   group on the specified server.  This is also possible with an
   equivalent 'news' URL, and the latter is better supported by user
   agents.  Example:`, () => {
    const uris = [
      'nntp://news.server.example/example.group.this',
      'news://news.server.example/example.group.this'
    ]
  })

  test.skip(`The form identifying <newsgroups> corresponds to the [RFC3977]
   <wildmat-pattern>, a newsgroup name with wildcards "*" and "?".  Any
   "?" has to be percent-encoded as "%3F" in this part of a URI.
   Examples (the first two are equivalent):`, () => {
    const uris = [
      'news://news.server.example/*',
      'news://news.server.example/',
      'news://wild.server.example/example.group.th%3Fse',
      'news:example.group.*',
      'news:example.group.this'
    ]
  })

  test.skip(`Here is an example of a mail to the <mailto:tools.discuss@ietf.org>
   list with "Message-ID" <p0624081dc30b8699bf9b@[10.20.30.108]>.

   <http://dir.gmane.org/gmane.ietf.tools> is one of the various list
   archives; it converts mail into Netnews articles.  The header of this
   article contains the following fields (among others):
   The "Xref" roughly indicates the 742nd article in newsgroup
   <news://news.gmane.org/gmane.ietf.tools> on this server.  An 'nntp'
   URL might be <nntp://news.gmane.org/gmane.ietf.tools/742>.  For
   details about the "Archived-At" URL, see [RFC5064].`, () => {
    const uri = 'news://news.gmane.org/gmane.ietf.tools'
  })

  test.skip(`The list software and list subscribers reading the list elsewhere
   can't predict a server-specific article number 742 in this archive.
   If they know this server, they can however guess the corresponding
   <news://news.gmane.org/p0624081dc30b8699bf9b@%5B10.20.30.108%5D> URL.`, () => {
    const uri = 'news://news.gmane.org/p0624081dc30b8699bf9b@%5B10.20.30.108%5D'
  })
})
